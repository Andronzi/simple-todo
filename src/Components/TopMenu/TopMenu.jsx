import Icons from "./Icons";
import Person from "./Person/Person";
import Search from "./Search/Search";

const TopMenu = () => {
  return (
    <div className="top-menu">
      <Person />
      <Search />
      <Icons />
    </div>
  );
};

export default TopMenu;
