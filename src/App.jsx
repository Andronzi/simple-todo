import TopMenu from "./Components/TopMenu/TopMenu";

function App() {
  return (
    <div className="App">
      <TopMenu />
    </div>
  );
}

export default App;
