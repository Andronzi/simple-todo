import notifications from "./images/notifications.png";
import settings from "./images/settings.png";

const Icons = () => {
  return (
    <div className="icons-block">
      <img
        className="icons-block__icon"
        src={notifications}
        alt="Уведомления"
      />
      <img className="icons-block__icon" src={settings} alt="Настройки" />
    </div>
  );
};

export default Icons;
