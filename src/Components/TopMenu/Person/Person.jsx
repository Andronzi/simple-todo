import avatar from "./avatar.png";

const Person = () => {
  return (
    <div className="person-block">
      <img className="person-block__image" src={avatar} alt="avatar" />
      <h2 className="person-block__name-title">Андрей Радионов</h2>
      <p className="person-block__email-title">
        andrey.radioniv.2003@gmail.com
      </p>
    </div>
  );
};

export default Person;
