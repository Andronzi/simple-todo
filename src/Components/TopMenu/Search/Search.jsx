import search from "./search.png";

const Search = () => {
  return (
    <div className="search-block">
      <input className="search-block__input" placeholder="Поиск задачи" />
      <img className="search-block__icon" src={search} alt="поиск" />
    </div>
  );
};

export default Search;
